#!/bin/bash

txt2man -d "${CHANGELOG_DATE}" -t SRSENB -s 1 srsenb.txt > srsenb.1
txt2man -d "${CHANGELOG_DATE}" -t SRSEPC -s 1 srsepc.txt > srsepc.1
txt2man -d "${CHANGELOG_DATE}" -t SRSUE -s 1 srsue.txt > srsue.1
